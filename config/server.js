const express   = require('express');
const EventEmitter = require('events');
const logger    = require('morgan');

//Routes
const userRouter = require('../src/routes/user.route');

// Inicializa o Express;
const app = express();

const emitter = new EventEmitter()
emitter.setMaxListeners(100)

// Global Configurations
app.set('port', process.env.PORT || 3000);

//  Middlewares
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

//  Routes
app.use('/', userRouter);


module.exports = app;