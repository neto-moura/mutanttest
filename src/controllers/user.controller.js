// const User = require('../models/user.model');
const request = require('request');

module.exports.users = {
    index: (req, res) => {

        try {
            //  faz a requisição
            MakeRequest(function (err, users) {
                //  filtra apenas Usuarios que tenham no endereço no atributo suite 
                //  a palavra Suite
                const Filter01 = users.filter(user => {

                    let suite = user.address.suite.toLowerCase() || '';

                    if (suite.includes('suite')) {
                        return true;
                    } else {
                        return false;
                    }

                })

                //  Filtra informações apenas que seram usadas
                const Filter02 = Filter01.map(user => {

                    userfiltered = {
                        name: user.name || '',
                        email: user.email || '',
                        company: user.company || '',
                        website: user.website || ''
                    }

                    //  retorna apenas valores informados
                    return userfiltered
                })

                //  Ordena por ordem alfabetica o nome da compania
                const sortedByCompany = Filter02.sort(sortCompany);

                res.status(200).json({
                    users: sortedByCompany
                });

                //Fim Metodo Index()
                return;
            })
        } catch (ex) {
            res.status(400).json({err:'Err',msg:'Problema nos Valores recebidos da URL, não estão seguindo o padrão'})
        }
    }// Index()
}// ExportFunction

//  Ordena de acordo com o Objeto, no caso de acordo com o nome da compania
function sortCompany(companyA, companyB) {
    if (companyA.company.name < companyB.company.name)
        return -1;
    if (companyA.company.name > companyB.company.name)
        return 1;
    return 0;
}// sortCompany

//  Faz a requisição no servidor com os dados Mocados (Usuarios)
function MakeRequest(callback) {

    const URL = 'https://jsonplaceholder.typicode.com'
    const relativePath = '/users'
    const method = 'GET'

    //  Formando a Request
    var options = {
        baseUrl: URL,
        uri: relativePath,
        method: method,
        json: true,
    };

    //  Faz a requisição
    request(options, function (err, response, body) {
        //  Retorna os valores para serem manipulados externamente por callback
        callback(err, body)
    });
}// MakeRequest