const express = require('express')
const router = express.Router();

const Controllers = require('../controllers/user.controller');

router.get('/', Controllers.users.index);
 
module.exports = router;