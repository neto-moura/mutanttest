const server = require('./config/server');

server.listen(server.get('port'), () => {
  console.log(`servidor rodando na porta ${server.get('port')}`);
});