# Mutant Test

## Imagem Docker

Comando para puxar a imagem do `Docker Hub`

```bash

docker pull netomourajs/test-mutant - #Imagem Publica

````

## Detalhes do Repositorio

Versão node Usada 8.10

### Comandos para rodar o App com Codigo do Repositorio

```javascript

// Rodando o Teste
npm test

// Rodando o App
npm start

```
